const fs = require('fs');

const figure_types = ['C', 'L', 'R', 'B']


function  check_if_positive(array){
    array.forEach(one_element=>{
        if(one_element<0){
            throw new Error("Can't be negative\n");
        }
    })
}
// @params ={
//      max_x:value,
//      min_x:value,
//      max_y:value,
//      min_y:value
// }
//
function check_coordinates(x, y, params) {
    if (x > params.max_x || x < params.min_x || y > params.max_y || y < params.min_y) {
        throw new Error("bad coordinates");
    }
}


let line_functions = {
    canvasArray: null,
    generate_draw_line: ([x, y, x1, y1], array, params) => {
        check_coordinates(x, y, params);
        check_coordinates(x1, y1, params);
        line_functions.canvasArray = array;
        var line_type = line_functions.get_line_type(x, y, x1, y1);
        switch (line_type) {
            case 'horizontal' : {
                line_functions.draw_gorizontal_line([x, y, x1, y1])
                break;
            }
            case 'vertical' : {
                line_functions.draw_vertical_line([x, y, x1, y1])
                break;
            }
            default: {

            }
        }
    },
    get_line_type: (x, y, x1, y1) => {
        if (y == y1) {
            return 'horizontal'
        } else if (x == x1) {
            return 'vertical'
        }
    },
    draw_gorizontal_line: ([x, y, x1, y1]) => {
        // swap if need because i need always x<x1 .can be function , i know =)
        if (x > x1) {
            let tmp = x1;
            x1 = x;
            x = tmp;
            tmp = y1;
            y1 = y;
            y = tmp;
        }
        while (x <= x1) {
            line_functions.canvasArray[y][x] = 'x';
            x++;
        }
    },
    draw_vertical_line: ([x, y, x1, y1]) => {
        // swap if need  because i need always y<y1. can be function , i know =)
        if (y > y1) {
            let tmp = x1;
            x1 = x;
            x = tmp;
            tmp = y1;
            y1 = y;
            y = tmp;
        }

        while (y <= y1) {
            line_functions.canvasArray[y][x] = 'x';
            y++;
        }
    }
}


class Editor {
    constructor(file_name) {
        this.file_name = file_name;
        this.draw_array = new Array();
        this.was_canvas = false;
        this.params = {
            max_x: -1,
            min_x: -1,
            max_y: -1,
            min_y: -1
        }

    }

    static draw() {
        return new Editor('FileData/elephant/input.txt').draw_file();
    }




    parse_one_line(line) {
        var [position_in_line, command_type_index] = this.find_command_type(line);
        var command_type = figure_types[command_type_index];

        if (command_type != 'C' && !this.was_canvas) {
            throw new Error("You can only draw if a canvas has been created.")
        }

        switch (command_type) {
            case 'C': {
                this.generate_draw_canvas(line.substring(position_in_line + 1).split(" ").map(number => parseInt(number)).filter(number => {
                    return Number.isInteger(number)
                }))
                // canvas exist
                this.was_canvas = true;
                this.draw_canvas();
                break;
            }
            case 'L': {
                line_functions.generate_draw_line(line.substring(position_in_line + 1).split(" ").map(number=>parseInt(number)).filter(number => {
                    return !isNaN(parseInt(number))
                }), this.draw_array, this.params)
                this.draw_canvas();
                break;
            }
            case 'R': {
                this.generate_rectangle(line.substring(position_in_line + 1).split(" ").map(number=>parseInt(number)).filter(number => {
                    return parseInt(number)
                }))
                this.draw_canvas();
                break;
            }
            case 'B': {
                // need, because i parse 2 number, then symbol ( solves problems with unnecessary characters )
                let number_count=0;
                this.bucket_fill(line.substring(position_in_line + 1).split(" ").filter(symbol => {
                    if (parseInt(symbol)) {
                        number_count++;
                        return parseInt(symbol)
                    } else if (symbol != ''&&number_count==2) {
                        return symbol
                    }
                }))
                break;
            }
        }
    }


    draw_file() {
        var lineReader = require('readline').createInterface({
            input: require('fs').createReadStream(this.file_name)
        });

        lineReader.on('line', (line) => {
            console.log('Line from file:', line);
            // fight with empty lines =)
            if (line.length == 0) return true;
            this.parse_one_line(line);
        });
    }

    find_command_type(line) {
        var position = 0;
        for (let one of line) {
            let index = figure_types.findIndex(x => {
                return x == one
            });
            if (!(index == -1)) {
                return [position, index];
            }
            position++;

            return [null, null]
        }
    }


    bucket_fill([y, x, nowSymbol])
        {
        y = parseInt(y);
        x = parseInt(x);
        console.log(y, x, nowSymbol);
        var fill_canvas = (x, y, prevSymbol, nowSymbol) => {

            // Сheck out coordinates beyond canvas  (remembering about borders)
            if (y < 1 || y >= this.draw_array[0].length - 1 || x < 1 || x >= this.draw_array.length - 1) {
                return false;
            }


            if (this.draw_array[x][y] != prevSymbol) {
                return false;
            }

            this.draw_array[x][y] = nowSymbol;


            fill_canvas(x - 1, y, prevSymbol, nowSymbol);
            fill_canvas(x, y + 1, prevSymbol, nowSymbol);
            fill_canvas(x + 1, y, prevSymbol, nowSymbol);
            fill_canvas(x, y - 1, prevSymbol, nowSymbol);

        }

        var previusSymbol = this.draw_array[x][y];
        console.log('now color = ', nowSymbol);
        this.draw_canvas();
        fill_canvas(x, y, previusSymbol, nowSymbol);
        this.draw_canvas();
    }

    generate_draw_canvas([width, height])
        {
         check_if_positive([width,height]);


        console.log("size = ", width, height);

        //   +2 because left and right borders
        let canvas_width = width + 2;
        //   +2 because top and bottom borders
            let canvas_height = height + 2;


        console.log("size after border = " ,canvas_width, canvas_height);

        this.draw_array = new Array(canvas_height);
        for (let i = 0; i < canvas_height; i++) {
            this.draw_array[i] = new Array(canvas_width);
        }

        // makes borders and fills with spaces other
        for (let i = 0; i < this.draw_array.length; i++) {
            for (let j = 0; j < this.draw_array[i].length; j++) {
                if (i == 0 || i == this.draw_array.length - 1) {
                    this.draw_array[i][j] = '-';
                }
                else if (j == 0 || j == this.draw_array[i].length - 1) {
                    this.draw_array[i][j] = '|'
                }
                else {
                    this.draw_array[i][j] = ' '
                }
            }
        }

        this.params.max_x = canvas_width - 2;
        this.params.min_x = 1;

        this.params.max_y = canvas_height - 2;
        this.params.min_y = 1;
    }

    draw_canvas()
        {
        for (let row of this.draw_array) {
            console.log(row.join(" "))
        }
    }

    generate_rectangle([x, y, x1, y1])
        {
        check_coordinates(x, y, this.params);
        check_coordinates(x1, y1, this.params);
        line_functions.draw_gorizontal_line([x, y, x1, y]);
        line_functions.draw_gorizontal_line([x, y1, x1, y1]);
        line_functions.draw_vertical_line([x, y, x, y1]);
        line_functions.draw_vertical_line([x1, y, x1, y1]);
    }

}


module.exports = Editor;